import React from "react";
import styles from "../../style/loginpage.module.css";

export default function LoginPage() {
  return (
    <div className={styles.container}>
      <div className="d-flex justify-content-center h-100">
        <div className={styles.card}>
          <div className="logo" style={{ backgroundColor: "rgb(255, 238, 0)" }}>
            <img className="navbar-brand" src="img/logo.png" alt="logo" />
          </div>
          <div className={styles.card_header}>
            <h3>Đăng nhập</h3>
          </div>
          <div className="card-bordy">
            <form>
              <div className="input-group form-group">
                <div className="input-group-prepend">
                  <span className="input-group-text">
                    <i className="fas fa-user" />
                  </span>
                </div>
                <input
                  type="text"
                  className="form-control"
                  id="username"
                  placeholder="Tên đăng nhập"
                />
              </div>
              <div className="input-group form-group">
                <div className="input-group-prepend">
                  <span className="input-group-text">
                    <i className="fas fa-key" />
                  </span>
                </div>
                <input
                  type="password"
                  className="form-control"
                  id="password"
                  placeholder="Mật khẩu"
                />
              </div>
              <div className="row align-items-center remember ml-2">
                <input className="mr-3" type="checkbox" />
                Lưu mật khẩu
              </div>
              <div className="mr-3 d-flex flex-row-reverse">
                <button className="btn btn-secondary">Login</button>
              </div>
            </form>
          </div>
          <div className="card-footer"></div>
        </div>
      </div>
    </div>
  );
}
